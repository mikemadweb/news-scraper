# News scraper
> Coding assignment done by Mikhail Bakulin

An awesome little tool for those who like to collect news as a hobby.

## How to run
To run the setup for news websites listed in sources.json:
```bash
make run
```
Check `output` directory for the article files.

To run the dev mode with the debugger:
```bash
make run_debug
```
Change the application configuration the way you want in `.env` file.
## How to run tests
The following shortcut could be used to execute tests:
```bash
make test
```
### How it works
![image info](./news_scraper.png)
Source in this context is a news website that would be a target for the scraper. Every source has a set of settings related to the scheduling, fetching, extract, and transforming the data. Scraper engine would start an independent scheduler for every source. Keeping in mind the rate limits and already scraped articles stored by the state, the scheduler would send the tasks to the pool of workers. Basically, there are two types of tasks to extract article links and to scrape the article content. 

A new task would be picked up by the next free worker in the pool. If there is no free worker task that would be queued until one of them would become free again. This queue operation implemented in a way to be non-blocking. Every worker delegates the processing to its own modules. In the case of scrape article worker, the processing includes fetching the data from the website, extract the meaningful content, limit the word count if needed, and store the result. Module implementation could be swapped easily. 

After the worker processing is finished, the result would be sent back to the response channel set by the scheduler. A successful result would update the state of the scheduler, while the error response would schedule a retry.

### What was left aside

These things could be further improved:

* Currently, the state of the scheduler would store more articles indefinitely. Some garbage collection is needed.
* Error handling just reset the state of the article, so if the error is not temporary, the article would be rescheduled indefinitely.
* Proxy servers could be used to overcome the rate limits.
* There is no way at the moment to timeout the worker in the pool if it got stuck.
