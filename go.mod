module bitbucket.org/mikemadweb/news-scraper

go 1.14

require (
	github.com/andybalholm/cascadia v1.2.0
	github.com/gammazero/deque v0.0.0-20200310222745-50fa758af896
	github.com/go-playground/validator/v10 v10.3.0
	github.com/golang/glog v0.0.0-20160126235308-23def4e6c14b
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/lukechampine/freeze v0.0.0-20160818180733-f514e08ae5a0
	github.com/microcosm-cc/bluemonday v1.0.2
	github.com/pkg/errors v0.9.1
	github.com/stretchr/testify v1.4.0
	golang.org/x/net v0.0.0-20181220203305-927f97764cc3
	golang.org/x/sys v0.0.0-20200523222454-059865788121 // indirect
)
