package source

import (
	"io"
	"reflect"
	"strings"
	"testing"
	"time"

	"bitbucket.org/mikemadweb/news-scraper/config/custom"
)

func TestJsonSettingsReader_Read(t *testing.T) {
	type args struct {
		reader io.Reader
	}
	tests := []struct {
		name    string
		args    args
		want    []*Settings
		wantErr bool
	}{
		{
			name: "simple read",
			args: args{
				reader: strings.NewReader("[\n  {\n    \"domain\": \"nos.nl\",\n    \"requestRatePerMinute\": 10,\n    \"list\": {\n      \"pages\": [\n        \"https://nos.nl/nieuws/\"\n      ],\n      \"scrapeInterval\": \"1m\",\n      \"articleUrlPattern\": \"^\\\\/(artikel|liveblog)\\\\/\\\\d+\\\\-[a-zA-Z0-9\\\\-]+\\\\.html\"\n    },\n    \"article\": {\n      \"scrapeInterval\": \"5m\",\n      \"bodyWordLimit\": 500,\n      \"htmlSelectors\": {\n        \"headline\": \"h1.liveblog-header__title,h1[class^=\\\"title_\\\"]\",\n        \"body\": \"ul[class^=\\\"liveblog_\\\"] > li:not(:last-child),div[class^=\\\"contentBlock_\\\"] > div:not(:last-child)\"\n      }\n    }\n  }\n]"),
			},
			want: []*Settings{
				{
					Domain: "nos.nl",
					RequestRatePerMinute: 10,
					List: List{
						Pages:             []custom.Url{
							custom.Url("https://nos.nl/nieuws/"),
						},
						ScrapeInterval:    custom.Duration(1* time.Minute),
						ArticleUrlPattern: "^\\/(artikel|liveblog)\\/\\d+\\-[a-zA-Z0-9\\-]+\\.html",
					},
					Article: Article{
						ScrapeInterval: custom.Duration(5* time.Minute),
						BodyWordLimit:  500,
						HtmlSelectors:  ArticleHtmlSelectors{
							Headline: "h1.liveblog-header__title,h1[class^=\"title_\"]",
							Body: "ul[class^=\"liveblog_\"] > li:not(:last-child),div[class^=\"contentBlock_\"] > div:not(:last-child)",
						},
					},
				},
			},
			wantErr: false,
		},
		{
			name: "invalid input read",
			args: args{
				reader: strings.NewReader("invalid input"),
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &JsonSettingsReader{}
			got, err := r.Read(tt.args.reader)
			if (err != nil) != tt.wantErr {
				t.Errorf("Read() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Read() got = %v, want %v", got, tt.want)
			}
		})
	}
}