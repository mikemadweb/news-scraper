package source

import (
	"bitbucket.org/mikemadweb/news-scraper/config/custom"
)

type Settings struct {
	Domain               string  `json:"domain" validate:"required"`
	RequestRatePerMinute uint32  `json:"requestRatePerMinute" validate:"required"`
	List                 List    `json:"list" validate:"required"`
	Article              Article `json:"article" validate:"required"`
}

type List struct {
	Pages             []custom.Url    `json:"pages" validate:"required"`
	ScrapeInterval    custom.Duration `json:"scrapeInterval" validate:"required"`
	ArticleUrlPattern string          `json:"articleUrlPattern" validate:"required"`
}

type Article struct {
	ScrapeInterval custom.Duration      `json:"scrapeInterval" validate:"required"`
	BodyWordLimit  uint32               `json:"bodyWordLimit" validate:"required"`
	HtmlSelectors  ArticleHtmlSelectors `json:"htmlSelectors" validate:"required"`
}

type ArticleHtmlSelectors struct {
	Headline string `json:"headline" validate:"required"`
	Body     string `json:"body" validate:"required"`
}
