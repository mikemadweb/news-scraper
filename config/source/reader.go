package source

import (
	"encoding/json"
	"io"
	"io/ioutil"

	"github.com/go-playground/validator/v10"
	"github.com/lukechampine/freeze"
	"github.com/pkg/errors"
)

type SettingsReader interface {
	Read(io.Reader) ([]Settings, error)
}

type JsonSettingsReader struct{}

func NewJsonSettingsReader() *JsonSettingsReader {
	return &JsonSettingsReader{}
}

func (r *JsonSettingsReader) Read(reader io.Reader) ([]*Settings, error) {
	var sources []*Settings
	data, err := ioutil.ReadAll(reader)
	if err != nil {
		return nil, errors.Wrap(err, "source sources read failed")
	}

	err = json.Unmarshal(data, &sources)
	if err != nil {
		return nil, errors.Wrap(err, "source sources unmarshal failed")
	}

	for _, settings := range sources {
		err = validator.New().Struct(settings)
		if err != nil {
			return nil, errors.Wrapf(err, "source %s validation error", settings.Domain)
		}
	}

	return freeze.Slice(sources).([]*Settings), nil
}
