package custom

import (
	"testing"
)

func TestUrl_UnmarshalJSON(t *testing.T) {
	type args struct {
		b []byte
	}
	tests := []struct {
		name    string
		u       Url
		args    args
		wantErr bool
	}{
		{
			name: "valid unmarshal",
			u:    Url("https://www.example.com/search"),
			args: args{
				b: []byte("\"https://www.example.com/search\""),
			},
			wantErr: false,
		},
		{
			name: "invalid unmarshal",
			u:    Url("https://www.example.com/search"),
			args: args{
				b: []byte("\"non-url\""),
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := tt.u.UnmarshalJSON(tt.args.b); (err != nil) != tt.wantErr {
				t.Errorf("UnmarshalJSON() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}