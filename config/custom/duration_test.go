package custom

import (
	"testing"
	"time"
)

func TestDuration_UnmarshalJSON(t *testing.T) {
	type args struct {
		b []byte
	}
	tests := []struct {
		name    string
		d       Duration
		args    args
		wantErr bool
	}{
		{
			name: "valid unmarshal",
			d:    Duration(1 * time.Second),
			args: args{
				b: []byte("\"1s\""),
			},
			wantErr: false,
		},
		{
			name: "invalid unmarshal",
			d:    Duration(1 * time.Second),
			args: args{
				b: []byte("\"non-duration\""),
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := tt.d.UnmarshalJSON(tt.args.b); (err != nil) != tt.wantErr {
				t.Errorf("UnmarshalJSON() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
