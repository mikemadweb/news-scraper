package custom

import (
	"encoding/json"
	"fmt"
	"net/url"

	"github.com/pkg/errors"
)

type Url string

func (u *Url) MarshalJSON() ([]byte, error) {
	return json.Marshal(u.String())
}

func (u *Url) UnmarshalJSON(b []byte) error {
	var v interface{}
	if err := json.Unmarshal(b, &v); err != nil {
		return errors.Wrapf(err, "url unmarshal failed for %s", string(b))
	}
	switch value := v.(type) {
	case string:
		tmp, err := url.Parse(value)
		if err != nil {
			return errors.Wrapf(err, "url parse failed for %s", string(b))
		}
		if tmp.Scheme == "" || tmp.Host == "" {
			return errors.New(fmt.Sprintf("malformed url passed: %s", string(b)))
		}
		*u = Url(tmp.String())
		return nil
	default:
		return errors.New("invalid data type for url")
	}
}

func (u Url) Unwrap() string {
	return string(u)
}

func (u Url) String() string {
	return u.Unwrap()
}