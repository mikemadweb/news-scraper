package custom

import (
	"encoding/json"
	"time"

	"github.com/pkg/errors"
)

type Duration time.Duration

func (d *Duration) MarshalJSON() ([]byte, error) {
	return json.Marshal(d.Unwrap().String())
}

func (d *Duration) UnmarshalJSON(b []byte) error {
	var v interface{}
	if err := json.Unmarshal(b, &v); err != nil {
		return errors.Wrapf(err, "duration unmarshal failed for %s", string(b))
	}
	switch value := v.(type) {
	case float64:
		*d = Duration(time.Duration(value))
		return nil
	case string:
		tmp, err := time.ParseDuration(value)
		if err != nil {
			return errors.Wrapf(err, "duration parse failed for %s", string(b))
		}
		*d = Duration(tmp)
		return nil
	default:
		return errors.New("invalid duration")
	}
}

func (d *Duration) Unwrap() time.Duration {
	return time.Duration(*d)
}
