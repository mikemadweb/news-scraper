package config

import (
	"io"
	"log"
	"os"
	"time"

	"github.com/kelseyhightower/envconfig"
	"github.com/lukechampine/freeze"
	"github.com/pkg/errors"

	"bitbucket.org/mikemadweb/news-scraper/config/source"
)

type Config struct {
	SourcesFilePath     string        `envconfig:"SCRAPER_SOURCES_FILE_PATH" required:"true"`
	OutputBaseDirectory string        `envconfig:"SCRAPER_OUTPUT_BASE_DIRECTORY" required:"true"`
	DebugMode           bool          `envconfig:"SCRAPER_DEBUG_MODE" default:"0"`
	SchedulerBufferSize uint8         `envconfig:"SCRAPER_SCHEDULER_BUFFER_SIZE" default:"10"`
	WorkerPoolSize      uint32        `envconfig:"SCRAPER_WORKER_POOL_SIZE" required:"true"`
	WorkerTimeout       time.Duration `envconfig:"SCRAPER_WORKER_TIMEOUT" required:"true"`
	ClientTimeout       time.Duration `envconfig:"SCRAPER_CLIENT_TIMEOUT" required:"true"`
}

var debugMode bool

func IsDebugMode() bool {
	return debugMode
}

func NewConfig() *Config {
	var config Config
	err := envconfig.Process("", &config)
	if err != nil {
		log.Fatalf("config init failed: %s", err)
	}
	debugMode = config.DebugMode

	return freeze.Object(&config).(*Config)
}

func ReadSourcesFromJsonFile(path string) (sources []*source.Settings, err error) {
	if _, err = os.Stat(path); os.IsNotExist(err) {
		err = errors.Wrapf(err, "sources file does not exist: %s", path)
		return
	}

	var data io.ReadCloser
	data, err = os.Open(path)
	if err != nil {
		err = errors.Wrapf(err, "sources file read failed: %s", err)
		return
	}

	defer func() {
		closeErr := data.Close()
		if closeErr != nil {
			err = errors.Wrapf(closeErr, "file closure failed")
			return
		}
	}()

	reader := source.NewJsonSettingsReader()
	sources, err = reader.Read(data)
	if err != nil {
		return
	}

	if len(sources) == 0 {
		err = errors.New("no sources were read from the file")
	}

	return
}
