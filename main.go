package main

import (
	"log"

	"github.com/golang/glog"

	"bitbucket.org/mikemadweb/news-scraper/app"
	"bitbucket.org/mikemadweb/news-scraper/config"
)

func main() {
	c := config.NewConfig()

	if config.IsDebugMode() {
		log.Printf("reading sources settings from %s", c.SourcesFilePath)
	}

	sources, err := config.ReadSourcesFromJsonFile(c.SourcesFilePath)
	if err != nil {
		glog.Fatalf("config error: %s", err.Error())
	}

	engine := app.NewEngine(c, sources)

	engine.RegisterSignalHandlers()
	if err := engine.Run(); err != nil {
		glog.Fatalf("engine error: %s", err.Error())
	}
}
