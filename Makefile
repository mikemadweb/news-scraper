.PHONY: fmt
fmt:
	@$(call PRINT_INFO, "Format")
	@go fmt $$(go list ./... | grep -v build | grep -v vendor | grep -v .git | grep -v .idea)

.PHONY: test
test:
	@$(call PRINT_INFO, "Run tests")
	@docker run -it --rm --env CGO_ENABLED=0 -v $(shell pwd):/app -w /app golang:1.14-alpine go test -v ./... | sed ''/PASS/s//$$(printf "\033[32mPASS\033[0m")/'' | sed ''/FAIL/s//$$(printf "\033[31mFAIL\033[0m")/''

.PHONY: run
run:
	@$(call PRINT_INFO, "Compile and start news scraper")
	@docker build -t news-scraper -f docker/go/Dockerfile .
	@docker run -it -v $(shell pwd)/output:/app/output --env-file .env news-scraper

.PHONY: run_debug
run_debug:
	@$(call PRINT_INFO, "Run news scraper in dev mode with debugger enabled")
	@docker inspect news-scraper-debug > /dev/null 2>&1 || docker build -t news-scraper-debug -f docker/go-debug/Dockerfile .
	@docker run -it --rm --env-file .env -v $(shell pwd):/app -p 2345:2345 news-scraper-debug

define PRINT_INFO
	echo "\033[1;48;5;33m$1 \033[0m"
endef