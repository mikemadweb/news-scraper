package app

import (
	"errors"
	"fmt"
	"os"
	"os/signal"
	"syscall"

	"bitbucket.org/mikemadweb/news-scraper/app/pool"
	"bitbucket.org/mikemadweb/news-scraper/app/pool/worker"
	"bitbucket.org/mikemadweb/news-scraper/app/scheduler"
	"bitbucket.org/mikemadweb/news-scraper/config"
	"bitbucket.org/mikemadweb/news-scraper/config/source"
)

type engine struct {
	sources                 []*source.Settings
	scheduler               scheduler.Scheduler
	scrapeArticleWorkerPool pool.Pool
	extractLinksWorkerPool  pool.Pool
}

func NewEngine(config *config.Config, sources []*source.Settings) *engine {
	return &engine{
		sources:                 sources,
		scheduler:               scheduler.NewScheduler(config),
		scrapeArticleWorkerPool: pool.NewPool(worker.NewScrapeArticleWorker(config), config.WorkerPoolSize),
		extractLinksWorkerPool:  pool.NewPool(worker.NewExtractLinksWorker(config), config.WorkerPoolSize),
	}
}

func (e *engine) Run() error {
	for task := range e.scheduler.Run(e.sources) {
		if _, ok := task.(worker.ExtractLinksTask); ok {
			e.extractLinksWorkerPool.Do(task)
		} else if _, ok := task.(worker.ScrapeArticleTask); ok {
			e.scrapeArticleWorkerPool.Do(task)
		} else {
			return errors.New(fmt.Sprintf("engine: unknown task type %T", task))
		}
	}

	return nil
}

func (e *engine) RegisterSignalHandlers() {
	c := make(chan os.Signal)
	signal.Notify(c, syscall.SIGINT, syscall.SIGTERM)

	go func() {
		<-c
		fmt.Println("*********************************\nShutdown signal received\n*********************************")
		e.scrapeArticleWorkerPool.Stop()
		e.extractLinksWorkerPool.Stop()

		for !e.extractLinksWorkerPool.IsStopped() || !e.scrapeArticleWorkerPool.IsStopped() {
		}
		fmt.Println("*********************************\nGraceful shutdown completed\n*********************************")
		os.Exit(0)
	}()
}
