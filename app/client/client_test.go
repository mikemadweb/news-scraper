package client

import (
	"net/http"
	"net/http/httptest"
	"reflect"
	"testing"
)

type HttpClient interface {
	Do(req *http.Request) (*http.Response, error)
}

type MockClient struct {
	DoFunc func(req *http.Request) (*http.Response, error)
}

func (m *MockClient) Do(req *http.Request) (*http.Response, error) {
	if m.DoFunc != nil {
		return m.DoFunc(req)
	}

	return &http.Response{}, nil
}

func Test_client_Get(t *testing.T) {
	srv := serverMock()
	defer srv.Close()

	type fields struct {
		httpClient *http.Client
	}
	type args struct {
		url string
	}
	tests := []struct {
		name         string
		fields       fields
		args         args
		wantResponse []byte
		wantErr      bool
	}{
		{
			name:         "fetch news",
			fields:       fields{
				http.DefaultClient,
			},
			args:         args{
				srv.URL,
			},
			wantResponse: []byte("news to fetch"),
			wantErr:      false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := &client{
				httpClient: tt.fields.httpClient,
			}
			gotResponse, err := c.Get(tt.args.url)
			if (err != nil) != tt.wantErr {
				t.Errorf("Get() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(gotResponse, tt.wantResponse) {
				t.Errorf("Get() gotResponse = %v, want %v", gotResponse, tt.wantResponse)
			}
		})
	}
}

func serverMock() *httptest.Server {
	handler := http.NewServeMux()
	handler.HandleFunc("/", usersMock)

	srv := httptest.NewServer(handler)

	return srv
}

func usersMock(w http.ResponseWriter, r *http.Request) {
	_, _ = w.Write([]byte("news to fetch"))
}