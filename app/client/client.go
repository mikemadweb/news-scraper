package client

import (
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/pkg/errors"

	"bitbucket.org/mikemadweb/news-scraper/config"
)

type Client interface {
	Get(url string) ([]byte, error)
}

type client struct {
	httpClient *http.Client
}

func NewClient(c *config.Config) *client {
	return &client{
		httpClient: &http.Client{
			Timeout: c.ClientTimeout,
		},
	}
}

func (c *client) Get(url string) (response []byte, err error) {
	var resp *http.Response
	resp, err = c.httpClient.Get(url)
	if err != nil {
		err = errors.Wrapf(err, "extract links worker : error during request to %s", url)
		return
	}
	if resp.StatusCode != http.StatusOK {
		err = errors.New(fmt.Sprintf("extract links worker : code=%d %s", resp.StatusCode, url))
		return
	}

	defer func() {
		closeErr := resp.Body.Close()
		if closeErr != nil {
			err = errors.Wrapf(closeErr, "error during response closure")
		}
	}()

	response, err = ioutil.ReadAll(resp.Body)

	return
}
