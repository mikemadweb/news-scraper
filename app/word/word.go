package word

import (
	"fmt"
	"regexp"
)

func Count(input string) uint32 {
	re := regexp.MustCompile(`\S+`)
	results := re.FindAllString(input, -1)

	return uint32(len(results))
}

func SliceAt(input string, nthWord uint32) string {
	re := regexp.MustCompile(fmt.Sprintf(`^\s*(?:\S+\s+){%d}\S+`, nthWord - 1))
	results := re.FindStringIndex(input)

	if len(results) == 0 {
		return input
	}

	return input[:results[1]]
}