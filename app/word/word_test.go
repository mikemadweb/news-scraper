package word

import (
	"testing"
)

func TestCount(t *testing.T) {
	type args struct {
		input string
	}
	tests := []struct {
		name string
		args args
		want uint32
	}{
		{
			name: "small simple text",
			args: args{
				input: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec in.",
					},
					want: 10,
		},
		{
			name: "long tricky text",
			args: args{
				input: "\n\nLorem ipsum dolor sit amet, consectetur adipiscing elit. In id eros arcu. Aliquam vitae elit sit amet orci elementum efficitur. Mauris mollis tellus nisl, eu iaculis lorem scelerisque nec. Aenean nec porta ex, id tristique leo. Suspendisse congue mattis tortor nec mollis. \t Phasellus porttitor orci vel mi consequat egestas. Nunc placerat egestas ipsum, ut blandit quam ultrices ac. Maecenas sit amet quam sit amet lectus tempor mattis.   Cras maximus pretium mattis. Aliquam dictum orci at auctor consectetur. Sed accumsan nisl eu neque aliquam, eget euismod urna fermentum. Donec dolor libero, egestas eu tortor vitae, mattis scelerisque sapien.\n\nMorbi ac convallis nunc. Aliquam erat volutpat. Donec cursus ante ac mauris porta tincidunt. Vivamus mattis odio a nisi fringilla, non feugiat nulla laoreet. Interdum et malesuada fames ac ante ipsum primis in faucibus. Etiam a nisi eleifend, sodales orci id, posuere erat. Duis ut. ",
			},
			want: 142,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Count(tt.args.input); got != tt.want {
				t.Errorf("Count() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestSliceAt(t *testing.T) {
	type args struct {
		input   string
		nthWord uint32
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "slice text",
			args: args{
				input: "Een bloemenkweker uit het Gelderse Heumen zit met 300.000 gladiolen in zijn maag. De bloemen waren bedoeld voor deelnemers aan de Nijmeegse Vierdaagse, maar die is vanwege de coronacrisis afgelast.",
				nthWord: 5,
			},
			want: "Een bloemenkweker uit het Gelderse",
		},
		{
			name: "slice tricky text",
			args: args{
				input: "\n\nLorem ipsum \t dolor sit amet, consectetur adipiscing elit.    In id eros arcu. Aliquam vitae elit sit amet orci elementum efficitur. Mauris mollis tellus nisl, \n\n\n eu iaculis lorem scelerisque nec. Aenean nec porta ex, id tristique leo. Suspendisse congue mattis tortor nec mollis.",
				nthWord: 10,
			},
			want: "\n\nLorem ipsum \t dolor sit amet, consectetur adipiscing elit.    In id",
		},
		{
			name: "slice short text",
			args: args{
				input: "Lorem ipsum ",
				nthWord: 10,
			},
			want: "Lorem ipsum ",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := SliceAt(tt.args.input, tt.args.nthWord); got != tt.want {
				t.Errorf("SliceAt() = %v, want %v", got, tt.want)
			}
		})
	}
}