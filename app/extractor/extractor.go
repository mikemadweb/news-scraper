package extractor

import (
	"bytes"
	"io"
	"regexp"
	"strings"

	"github.com/andybalholm/cascadia"
	"github.com/microcosm-cc/bluemonday"
	"github.com/pkg/errors"
	"golang.org/x/net/html"

	"bitbucket.org/mikemadweb/news-scraper/config"
)

type Extractor interface {
	ExtractLinks(response []byte, urlPattern string) ([]string, error)
	ExtractText(response []byte, selector string) (string, error)
}

type htmlExtractor struct {
}

func NewHtmlExtractor(config *config.Config) *htmlExtractor {
	return &htmlExtractor{}
}

func (e *htmlExtractor) ExtractLinks(response []byte, urlPattern string) ([]string, error) {
	var links []string
	matches, err := e.findByCssSelector(bytes.NewReader(response), "a")
	if err != nil {
		return nil, err
	}
	for _, match := range matches {
		for _, attr := range match.Attr {
			if attr.Key == "href" {
				isPatternMatch, err := regexp.MatchString(urlPattern, attr.Val)
				if err != nil {
					return nil, errors.Wrapf(err, "regex match failed")
				}
				if isPatternMatch {
					links = append(links, attr.Val)
				}
			}
		}
	}

	return links, nil
}

func (e *htmlExtractor) ExtractText(response []byte, selector string) (string, error) {
	matches, err := e.findByCssSelector(bytes.NewReader(response), selector)
	if err != nil {
		return "", err
	}

	buf := bytes.NewBufferString("")
	for _, match := range matches {
		if err := html.Render(buf, match); err != nil {
			return "", errors.Wrap(err, "failed to print html as text")
		}
	}

	return e.clearOutHtml(buf.String()), nil
}

func (e *htmlExtractor) findByCssSelector(haystack io.Reader, selector string) ([]*html.Node, error) {
	s, err := cascadia.Compile(selector)
	if err != nil {
		return nil, errors.Wrap(err, "compiling link selector failed")
	}

	document, err := html.Parse(haystack)
	if err != nil {
		return nil, errors.Wrap(err, "html parsing failed")
	}

	return s.MatchAll(document), nil
}

func (e *htmlExtractor) clearOutHtml(content string) string {
	sanitizer := bluemonday.StrictPolicy()
	withoutTags := sanitizer.Sanitize(content)

	space := regexp.MustCompile(`\s+`)
	withoutDuplicateSpaces := space.ReplaceAllString(withoutTags, " ")

	withoutHtmlEntities := html.UnescapeString(withoutDuplicateSpaces)

	return strings.TrimSpace(withoutHtmlEntities)
}
