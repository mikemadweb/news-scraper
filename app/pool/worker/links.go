package worker

import (
	"fmt"
	"log"
	"time"

	"github.com/pkg/errors"

	"bitbucket.org/mikemadweb/news-scraper/app/client"
	"bitbucket.org/mikemadweb/news-scraper/app/extractor"
	"bitbucket.org/mikemadweb/news-scraper/app/storage"
	"bitbucket.org/mikemadweb/news-scraper/config"
)

const extractLinksLogPrefix = "extract links worker"

type ExtractLinksTask struct {
	Input  ExtractLinksInput
	Output chan ExtractLinksOutput
	Error chan error
}

type ExtractLinksInput struct {
	PageUrl           string
	ArticleUrlPattern string
}

type ExtractLinksOutput struct {
	PageUrl      string
	ArticleLinks []string
}

type ExtractLinksWorker struct {
	timeout      time.Duration
	client       client.Client
	extractor    extractor.Extractor
	storage      storage.Storage
}

func NewExtractLinksWorker(config *config.Config) *ExtractLinksWorker {
	return &ExtractLinksWorker{
		timeout:   config.WorkerTimeout,
		client:    client.NewClient(config),
		extractor: extractor.NewHtmlExtractor(config),
		storage:   storage.NewJsonFileStorage(config),
	}
}

func (w *ExtractLinksWorker) Execute(task interface{}) {
	t, ok := task.(ExtractLinksTask)
	if !ok {
		panic(fmt.Sprintf("%s: unexpected task type %T", extractLinksLogPrefix, task))
	}

	defer func() {
		if r := recover(); r != nil {
			if err, ok := r.(error); ok {
				t.Error <- err
			}
			if str, ok := r.(string); ok {
				t.Error <- errors.New(str)
			}
		}
	}()

	if config.IsDebugMode() {
		log.Printf("%s: task received - %s\n", extractLinksLogPrefix, t.Input.PageUrl)
	}

	response, err := w.client.Get(t.Input.PageUrl)
	if err != nil {
		t.Error <- errors.Wrapf(err, "%s: client error", extractLinksLogPrefix)
		return
	}

	links, err := w.extractor.ExtractLinks(response, t.Input.ArticleUrlPattern)
	if err != nil {
		t.Error <- errors.Wrapf(err, "%s: link extractor", extractLinksLogPrefix)
		return
	}

	if config.IsDebugMode() {
		log.Printf("%s: %d article links extracted - %s\n", extractLinksLogPrefix, len(links), t.Input.PageUrl)
	}

	t.Output <- ExtractLinksOutput{
		PageUrl:      t.Input.PageUrl,
		ArticleLinks: links,
	}
}
