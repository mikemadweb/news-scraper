package worker

import (
	"fmt"
	"log"
	"time"

	"github.com/pkg/errors"

	"bitbucket.org/mikemadweb/news-scraper/app/client"
	"bitbucket.org/mikemadweb/news-scraper/app/extractor"
	"bitbucket.org/mikemadweb/news-scraper/app/storage"
	"bitbucket.org/mikemadweb/news-scraper/app/word"
	"bitbucket.org/mikemadweb/news-scraper/config"
)

const scrapeArticleLogPrefix = "scrape article worker"

type ScrapeArticleTask struct {
	Input  ScrapeArticleInput
	Output chan ScrapeArticleOutput
	Error chan error
}

type ScrapeArticleInput struct {
	PageUrl          string
	BodyWordLimit    uint32
	HeadlineSelector string
	BodySelector     string
}

type ScrapeArticleOutput struct {
	ArticleUrl string
	Version    string
}

type ScrapeArticleWorker struct {
	timeout   time.Duration
	client    client.Client
	extractor extractor.Extractor
	storage   storage.Storage
}

func NewScrapeArticleWorker(config *config.Config) *ScrapeArticleWorker {
	return &ScrapeArticleWorker{
		timeout:   config.WorkerTimeout,
		client:    client.NewClient(config),
		extractor: extractor.NewHtmlExtractor(config),
		storage:   storage.NewJsonFileStorage(config),
	}
}

func (w *ScrapeArticleWorker) Execute(task interface{}) {
	t, ok := task.(ScrapeArticleTask)
	if !ok {
		panic(fmt.Sprintf("scrape article worker: unexpected task type %T", task))
	}

	defer func() {
		if r := recover(); r != nil {
			if err, ok := r.(error); ok {
				t.Error <- err
			}
			if str, ok := r.(string); ok {
				t.Error <- errors.New(str)
			}
		}
	}()

	if config.IsDebugMode() {
		log.Printf("%s: task received - %s\n", scrapeArticleLogPrefix, t.Input.PageUrl)
	}

	response, err := w.client.Get(t.Input.PageUrl)
	if err != nil {
		t.Error <- errors.Wrapf(err, "%s: client error", scrapeArticleLogPrefix)
		return
	}

	headline, err := w.extractor.ExtractText(response, t.Input.HeadlineSelector)
	if err != nil {
		t.Error <- errors.Wrapf(err, "%s: headline extractor", scrapeArticleLogPrefix)
		return
	}
	if len(headline) == 0 {
		t.Error <- errors.New("headline was not extracted for " + t.Input.PageUrl)
		return
	}

	body, err := w.extractor.ExtractText(response, t.Input.BodySelector)
	if err != nil {
		t.Error <- errors.Wrapf(err, "%s: body extractor", scrapeArticleLogPrefix)
		return
	}
	if len(body) == 0 {
		t.Error <- errors.New("body was not extracted for " + t.Input.PageUrl)
		return
	}

	wordCount := word.Count(body)
	isBodyLimited := wordCount >= t.Input.BodyWordLimit
	if t.Input.BodyWordLimit != 0 {
		if config.IsDebugMode() {
			log.Printf(
				"%s: article body extracted, word count: %d, is body limited: %t - %s\n",
				scrapeArticleLogPrefix,
				wordCount,
				isBodyLimited,
				t.Input.PageUrl)
		}
		body = word.SliceAt(body, t.Input.BodyWordLimit)
	}

	version, err := w.storage.SaveArticle(storage.Article{
		Url:           t.Input.PageUrl,
		Headline:      headline,
		Body:          body,
		IsBodyLimited: isBodyLimited,
	})
	if err != nil {
		t.Error <- errors.Wrapf(err, "%s: storage", scrapeArticleLogPrefix)
		return
	}

	if config.IsDebugMode() {
		log.Printf("%s: article persisted - %s\n", scrapeArticleLogPrefix, t.Input.PageUrl)
	}

	t.Output <- ScrapeArticleOutput{
		ArticleUrl: t.Input.PageUrl,
		Version:    version,
	}
}
