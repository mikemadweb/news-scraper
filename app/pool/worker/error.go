package worker

type Error struct {
	Err error
	Url string
}

func (e Error) Error() string {
	return e.Err.Error()
}
