package pool

import (
	"fmt"
	"log"
	"sync"
	"sync/atomic"

	"github.com/gammazero/deque"

	"bitbucket.org/mikemadweb/news-scraper/config"
)

type Pool interface {
	Do(interface{})
	GetWaitQueueLength() int
	Stop()
	IsStopped() bool
}

type Worker interface {
	Execute(interface{})
}

func NewPool(worker Worker, size uint32) *pool {
	if size == 0 {
		log.Fatalf("worker pool could not be empty")
	}

	pool := &pool{
		size:          size,
		workerType:    fmt.Sprintf("%T", worker),
		poolChannel:   make(chan interface{}, 1),
		workerChannel: make(chan interface{}),
		stopChannel:   make(chan struct{}),
	}

	go pool.startPool()

	var i uint32
	for i = 0; i < pool.size; i++ {
		go startWorker(worker, pool.workerChannel)
	}

	return pool
}

type pool struct {
	size            uint32
	workerType      string
	poolChannel     chan interface{}
	workerChannel   chan interface{}
	waitQueue       deque.Deque
	waitQueueLength int32
	stopChannel     chan struct{}
	stopOnce        sync.Once
	isStopped       bool
}

func (p *pool) Do(task interface{}) {
	p.poolChannel <- task
}

func (p *pool) GetWaitQueueLength() int {
	return int(atomic.LoadInt32(&p.waitQueueLength))
}

func (p *pool) Stop() {
	p.stopOnce.Do(func() {
		close(p.poolChannel)
		<-p.stopChannel
		p.isStopped = true
	})
}

func (p *pool) IsStopped() bool {
	return p.isStopped
}

func (p *pool) startPool() {
	defer close(p.stopChannel)

DispatchLoop:
	for {
		if p.GetWaitQueueLength() != 0 {
			select {
			case task, ok := <-p.poolChannel:
				if !ok {
					break DispatchLoop
				}
				p.pushToWaitQueue(task)
			case p.workerChannel <- p.peekWaitQueue():
				p.popWaitQueue()
			}
			continue
		}
		task, ok := <-p.poolChannel
		if !ok {
			break DispatchLoop
		}
		select {
		case p.workerChannel <- task:
		default:
			p.pushToWaitQueue(task)
		}
	}

	if config.IsDebugMode() {
		log.Printf("%s worker pool : stopped\n", p.workerType)
	}
	var i uint32
	for i = 0; i < p.size; i++ {
		p.workerChannel <- nil
	}
}

func startWorker(worker Worker, workerQueue chan interface{}) {
	for task := range workerQueue {
		if task == nil {
			return
		}

		worker.Execute(task)
	}
}

func (p *pool) pushToWaitQueue(task interface{}) {
	p.waitQueue.PushBack(task)
	atomic.StoreInt32(&p.waitQueueLength, int32(p.waitQueue.Len()))
}

func (p *pool) popWaitQueue() interface{} {
	task := p.waitQueue.PopFront()
	atomic.StoreInt32(&p.waitQueueLength, int32(p.waitQueue.Len()))

	return task
}

func (p *pool) peekWaitQueue() interface{} {
	return p.waitQueue.Front()
}
