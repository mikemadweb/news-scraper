package storage

import (
	"reflect"
	"testing"
)

func Test_jsonFileStorage_encodeJson(t *testing.T) {
	type fields struct {
		baseDirectory string
	}
	type args struct {
		input Article
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []byte
		wantErr bool
	}{
		{
			name: "sample article encode",
			args: args{
				Article{
					Url:      "https://labs.spotify.com/2014/03/27/spotify-engineering-culture-part-1/",
					Version:  "e0a48bbbf6055ce128a01eea9030481b",
					Headline: "Spotify engineering culture (part 1)",
					Body:     "This is a journey in progress, not a journey completed, and there’s a lot of variation from squad to squad. So the stuff in the video isn’t all true for all squads all the time, but it appears to be mostly true for most squads most of the time :o)",
				},
			},
			want: []byte("{\n    \"url\": \"https://labs.spotify.com/2014/03/27/spotify-engineering-culture-part-1/\",\n    \"version\": \"e0a48bbbf6055ce128a01eea9030481b\",\n    \"headline\": \"Spotify engineering culture (part 1)\",\n    \"body\": \"This is a journey in progress, not a journey completed, and there’s a lot of variation from squad to squad. So the stuff in the video isn’t all true for all squads all the time, but it appears to be mostly true for most squads most of the time :o)\",\n    \"isBodyLimited\": false\n}"),
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &jsonFileStorage{
				baseDirectory: tt.fields.baseDirectory,
			}
			got, err := s.encodeJson(tt.args.input)
			if (err != nil) != tt.wantErr {
				t.Errorf("encodeJson() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("encodeJson() got = %s, want %s", got, tt.want)
			}
		})
	}
}

func Test_jsonFileStorage_resolveSubDirectory(t *testing.T) {
	type fields struct {
		baseDirectory string
	}
	type args struct {
		articleUrl string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    string
		wantErr bool
	}{
		{
			name: "valid article url",
			args: args{
				articleUrl: "https://labs.spotify.com/2014/03/27/spotify-engineering-culture-part-1/",
			},
			want:    "labs_spotify_com",
			wantErr: false,
		},
		{
			name: "invalid article url",
			args: args{
				articleUrl: "not a url hahaha",
			},
			want:    "",
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &jsonFileStorage{
				baseDirectory: tt.fields.baseDirectory,
			}
			got, err := s.resolveSubDirectory(tt.args.articleUrl)
			if (err != nil) != tt.wantErr {
				t.Errorf("resolveSubDirectory() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("resolveSubDirectory() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_jsonFileStorage_resolveVersion(t *testing.T) {
	type fields struct {
		baseDirectory string
	}
	type args struct {
		headline string
		body     string
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   string
	}{
		{
			name: "short article",
			args: args{
				headline: "Breaking news!",
				body:     "Breaking news body!",
			},
			want: "fe3907f98cfdc8e9e8b37afae88f1f30",
		},
		{
			name: "long article",
			args: args{
				headline: "Coronacijfers van 1 juni: Veluwse gemeenten telden veel opnames",
				body:     "Gemeenten in en om de Veluwe zijn afgelopen maand relatief zwaar getroffen door het coronavirus. Afgezet tegen het aantal inwoners telden de gemeenten Heerde, Nunspeet en Epe het grootste aantal ziekenhuisopnames. Ook Harderwijk werd relatief zwaar getroffen, net als Hattem, Ermelo en Voorst. In de top-tien van zwaarst getroffen gemeenten, afgezet tegen het aantal inwoners, staan maar twee gemeenten van buiten de Veluwe: Tilburg en het naastgelegen Oisterwijk. De meeste doden, afgezet tegen het aantal inwoners, vielen afgelopen maand in Weesp, Zwijndrecht en Boxtel. Daarbij moet worden aangetekend dat het aantal doden in bijvoorbeeld Heerde veel hoger is dan het RIVM registreerde: waar dat instituut tijdens de hele pandemie 28 doden in Heerde telde, heeft dagblad De Gelderlander het over 60 tot 80 doden. IC-opnames in gemeenten per 10.000 inwoners NOS 9 opnames Het RIVM meldde intussen 9 nieuwe ziekenhuisopnames, even veel als gemiddeld in de afgelopen week. Het aantal geregistreerde doden nam toe met 6; het gemiddelde afgelopen zeven dagen lag op 18 doden per dag. Vooral het aantal ziekenhuisopnames is voor het RIVM een belangrijke graadmeter voor de verspreiding van het virus. Sinds de start van de coronacrisis zijn door het RIVM 11.744 opnames gemeld. Bijna zesduizend mensen (5.962 patiënten) zijn door het RIVM als coronadode geteld. Het werkelijke aantal coronadoden ligt hoger omdat alleen geteste patiënten worden meegeteld. Het RIVM wijst er daarbij op dat de dagelijks gemelde cijfers kunnen achterlopen: vooral sterfgevallen worden soms pas na een aantal dagen doorgegeven door de GGD'en. Als dat op sommige dagen meer gebeurt dan gebruikelijk, ontstaan er pieken in de grafiek die het beeld vertekenen. Op weekenddagen kan de vertraging extra groot zijn, waardoor op de dagen daarna doorgaans een lager aantal wordt gemeld dan de rest van de week. RIVM-cijfers 1 juni NOS Op de intensive care blijft het relatief rustig, met in totaal 645 patiënten, waarvan 158 met het coronavirus. De 'coronabezetting' op de IC's daarmee één lager dan gisteren. Eén Nederlandse coronapatiënt ligt nog op een Duitse IC. Het aantal patiënten op de IC zonder corona daalde met 14 naar 488. Gisteren noemde Ernst Kuipers van het Landelijk Netwerk Acute Zorg het aantal niet-coronapatiënten laag. \"Huisartsen en ziekenhuizen hebben kennelijk langer nodig om de reguliere zorg weer op te starten\", ze hij toen. Na veel geplande operaties moeten patiënten nog een tijdje op de IC liggen. Door het coronavirus zijn veel reguliere operaties niet doorgegaan. IC-cijfers van 1 juni NOS",
			},
			want: "525d18a4887b8bde31d0d5c98298b90d",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &jsonFileStorage{
				baseDirectory: tt.fields.baseDirectory,
			}
			if got := s.resolveVersion(tt.args.headline, tt.args.body); got != tt.want {
				t.Errorf("resolveVersion() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_jsonFileStorage_resolveFilename(t *testing.T) {
	type fields struct {
		baseDirectory string
	}
	type args struct {
		url string
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   string
	}{
		{
			name: "simple article url",
			args: args{
				url: "https://labs.spotify.com/2014/03/27/spotify-engineering-culture-part-1/",
			},
			want: "e0a48bbbf6055ce128a01eea9030481b.json",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &jsonFileStorage{
				baseDirectory: tt.fields.baseDirectory,
			}
			if got := s.resolveFilename(tt.args.url); got != tt.want {
				t.Errorf("resolveFilename() = %v, want %v", got, tt.want)
			}
		})
	}
}
