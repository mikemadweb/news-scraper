package storage

import (
	"crypto/md5"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/url"
	"os"
	"path/filepath"
	"strings"

	"github.com/pkg/errors"

	"bitbucket.org/mikemadweb/news-scraper/config"
)

const filePermissions = 0644

type Storage interface {
	SaveArticle(Article) (version string, err error)
}

type jsonFileStorage struct {
	baseDirectory string
}

type Article struct {
	Url           string `json:"url"`
	Version       string `json:"version"`
	Headline      string `json:"headline"`
	Body          string `json:"body"`
	IsBodyLimited bool   `json:"isBodyLimited"`
}

func NewJsonFileStorage(config *config.Config) *jsonFileStorage {
	return &jsonFileStorage{
		baseDirectory: config.OutputBaseDirectory,
	}
}

func (s *jsonFileStorage) SaveArticle(article Article) (version string, err error) {
	var subdirectory string
	subdirectory, err = s.resolveSubDirectory(article.Url)
	if err != nil {
		return
	}

	directory := filepath.Join(s.baseDirectory, subdirectory)
	err = os.MkdirAll(directory, os.ModePerm)
	if err != nil {
		err = errors.Wrap(err, "storage unable to create directory")
		return
	}

	var data []byte
	version = s.resolveVersion(article.Headline, article.Body)
	article.Version = version

	data, err = s.encodeJson(article)

	var path string
	path = filepath.Join(directory, s.resolveFilename(article.Url))
	err = ioutil.WriteFile(path, data, filePermissions)
	if err != nil {
		err = errors.Wrap(err, "storage: file write failed")
		return
	}

	return
}

func (s *jsonFileStorage) encodeJson(input Article) ([]byte, error) {
	data, err := json.MarshalIndent(input, "", "    ")
	if err != nil {
		return nil, errors.Wrap(err, "storage: json marshal failed")
	}

	return data, nil
}

func (s *jsonFileStorage) resolveFilename(url string) string {
	return fmt.Sprintf("%x.json", md5.Sum([]byte(url)))
}

func (s *jsonFileStorage) resolveVersion(headline, body string) string {
	return fmt.Sprintf("%x", md5.Sum([]byte(headline+body)))
}

func (s *jsonFileStorage) resolveSubDirectory(articleUrl string) (string, error) {
	u, err := url.Parse(articleUrl)
	if err != nil {
		return "", errors.Wrap(err, "unable to parse article url")
	}
	if u.Host == "" {
		return "", errors.New("invalid url")
	}

	subDirectory := strings.ReplaceAll(u.Host, ".", "_")

	return subDirectory, nil
}
