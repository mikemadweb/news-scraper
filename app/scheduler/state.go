package scheduler

import (
	"container/heap"
	"fmt"
	"log"
	"net/url"
	"time"

	"github.com/gammazero/deque"
	"github.com/pkg/errors"

	"bitbucket.org/mikemadweb/news-scraper/app/pool/worker"
	"bitbucket.org/mikemadweb/news-scraper/config"
	"bitbucket.org/mikemadweb/news-scraper/config/source"
)

type state struct {
	sourceSettings      *source.Settings
	listPagesState      *listPagesState
	newArticleQueue     deque.Deque
	visitedArticleQueue *priorityQueue
	foundArticleMap     map[string]string
}

func newState(settings *source.Settings) *state {
	settingsListPages := settings.List.Pages
	if len(settingsListPages) == 0 {
		log.Fatalf("%s: no list pages passed", settings.Domain)
	}

	priorityQueue := make(priorityQueue, 0)
	heap.Init(&priorityQueue)

	var listPages []*listPageState
	for _, settingsListPage := range settingsListPages {
		listPages = append(listPages, &listPageState{
			url: settingsListPage.String(),
		})
	}

	return &state{
		sourceSettings: settings,
		listPagesState: &listPagesState{
			listPages: listPages,
		},
		newArticleQueue:     deque.Deque{},
		visitedArticleQueue: &priorityQueue,
		foundArticleMap:     make(map[string]string),
	}
}

type listPagesState struct {
	listPages          []*listPageState
	currentPageCounter int
}

type listPageState struct {
	url       string
	visitedAt int64
}

type articleState struct {
	url       string
	visitedAt int64
	position  int
}

func (s *state) StoreVisitedArticle(output worker.ScrapeArticleOutput) error {
	previousVersion, exist := s.foundArticleMap[output.ArticleUrl]
	if !exist {
		return errors.New("article not in the found map: " + output.ArticleUrl)
	}

	if previousVersion == output.Version {
		if config.IsDebugMode() {
			log.Printf(
				"%s: article was not changed, would not be scaped again [length=%d]",
				s.getLoggerPrefix(),
				s.visitedArticleQueue.Len())
		}

		return nil
	}

	s.foundArticleMap[output.ArticleUrl] = output.Version
	heap.Push(s.visitedArticleQueue, &articleState{
		url:       output.ArticleUrl,
		visitedAt: time.Now().UnixNano(),
	})

	if config.IsDebugMode() {
		result := "article was changed"
		if previousVersion == "" {
			result = "article read first time"
		}
		log.Printf(
			"%s: %s, adding to the visited article queue again [length=%d]",
			s.getLoggerPrefix(),
			result,
			s.visitedArticleQueue.Len())
	}

	return nil
}

func (s *state) StoreNewArticleLinks(output worker.ExtractLinksOutput) error {
	allArticleLinks, err := s.resolveRelativeLinks(output.PageUrl, output.ArticleLinks)
	if err != nil {
		return errors.Wrapf(err, "link resolve failed")
	}

	newArticleLinks, err := s.filterNewArticleLinks(allArticleLinks)
	if err != nil {
		return errors.Wrapf(err, "link list filter failed")
	}

	filteredOutCount := len(allArticleLinks) - len(newArticleLinks)
	if config.IsDebugMode() && filteredOutCount != 0 {
		log.Printf("%s: %d links filtered out", s.getLoggerPrefix(), filteredOutCount)
	}

	for _, listPage := range s.listPagesState.listPages {
		if listPage.url == output.PageUrl {
			listPage.visitedAt = time.Now().UnixNano()
		}
	}

	s.updateCurrentListPageCounter()
	if len(newArticleLinks) != 0 {
		for _, articleUrl := range newArticleLinks {
			s.newArticleQueue.PushBack(&articleState{
				url: articleUrl,
			})
		}

		if config.IsDebugMode() {
			log.Printf(
				"%s: %d new links added to the new article queue [length=%d]",
				s.getLoggerPrefix(),
				len(newArticleLinks),
				s.newArticleQueue.Len())
		}
	}

	return nil
}

func (s *state) GetCurrentListPage() *listPageState {
	return s.listPagesState.listPages[s.listPagesState.currentPageCounter]
}

func (s *state) PeekCurrentNewArticle() *articleState {
	if s.newArticleQueue.Len() == 0 {
		return nil
	}

	return s.newArticleQueue.Front().(*articleState)
}

func (s *state) GetCurrentNewArticle() *articleState {
	if s.newArticleQueue.Len() == 0 {
		return nil
	}

	if config.IsDebugMode() {
		log.Printf(
			"%s: article released from the new article queue [length=%d]",
			s.getLoggerPrefix(),
			s.newArticleQueue.Len())
	}

	return s.newArticleQueue.PopFront().(*articleState)
}

func (s *state) PeekCurrentVisitedArticle() *articleState {
	if s.visitedArticleQueue.Len() == 0 {
		return nil
	}

	return s.visitedArticleQueue.Front()
}

func (s *state) GetCurrentVisitedArticle() *articleState {
	if s.visitedArticleQueue.Len() == 0 {
		return nil
	}

	if config.IsDebugMode() {
		log.Printf(
			"%s: article released from the visited article queue [length=%d]",
			s.getLoggerPrefix(),
			s.visitedArticleQueue.Len())
	}

	return heap.Pop(s.visitedArticleQueue).(*articleState)
}

func (s *state) ResetArticle(articleUrl string) {
	if _, exist := s.foundArticleMap[articleUrl]; !exist {
		return
	}

	delete(s.foundArticleMap, articleUrl)
}

func (s *state) resolveRelativeLinks(baseUrl string, links []string) ([]string, error) {
	base, err := url.Parse(baseUrl)
	if err != nil {
		return nil, errors.Wrapf(err, "base url parsing failed for %s", baseUrl)
	}

	var resolvedLinks []string
	for _, link := range links {
		u, err := url.Parse(link)
		if err != nil {
			return nil, errors.Wrapf(err, "link parsing failed for %s", link)
		}

		resolvedLinks = append(resolvedLinks, base.ResolveReference(u).String())
	}

	return resolvedLinks, nil
}

func (s *state) filterNewArticleLinks(articleLinks []string) ([]string, error) {
	var newLinks []string
	for _, link := range articleLinks {
		if _, exist := s.foundArticleMap[link]; !exist {
			s.foundArticleMap[link] = ""
			newLinks = append(newLinks, link)
		}
	}

	return newLinks, nil
}

func (s *state) updateCurrentListPageCounter() {
	nextPageIndex := s.listPagesState.currentPageCounter + 1
	if nextPageIndex >= len(s.listPagesState.listPages) {
		nextPageIndex = 0
	}
	s.listPagesState.currentPageCounter = nextPageIndex
}

func (s *state) getLoggerPrefix() string {
	return fmt.Sprintf("scheduler state [%s]", s.sourceSettings.Domain)
}
