package scheduler

type priorityQueue []*articleState

func (pq priorityQueue) Len() int { return len(pq) }

func (pq priorityQueue) Less(i, j int) bool {
	return pq[i].visitedAt < pq[j].visitedAt
}

func (pq priorityQueue) Swap(i, j int) {
	pq[i], pq[j] = pq[j], pq[i]
	pq[i].position = i
	pq[j].position = j
}

func (pq priorityQueue) Front() *articleState {
	return pq[0]
}

func (pq *priorityQueue) Push(x interface{}) {
	n := len(*pq)
	item := x.(*articleState)
	item.position = n
	*pq = append(*pq, item)
}

func (pq *priorityQueue) Pop() interface{} {
	old := *pq
	n := len(old)
	item := old[n-1]
	old[n-1] = nil  // avoid memory leak
	item.position = -1 // for safety
	*pq = old[0 : n-1]
	return item
}