package scheduler

import (
	"log"

	"bitbucket.org/mikemadweb/news-scraper/config"
	"bitbucket.org/mikemadweb/news-scraper/config/source"
)

type Scheduler interface {
	Run([]*source.Settings) <-chan interface{}
}

type facade struct {
	taskChannel chan interface{}
	errorChannel chan error
}

func NewScheduler(config *config.Config) *facade {
	return &facade{
		taskChannel: make(chan interface{}, config.SchedulerBufferSize),
	}
}

func (f *facade) Run(sources []*source.Settings) <-chan interface{} {
	if config.IsDebugMode() {
		log.Printf("scheduler: started for %d sources", len(sources))
	}

	for _, settings := range sources {
		sourceScheduler := NewSourceScheduler(settings)
		go sourceScheduler.scheduleSend(f.taskChannel)
		go sourceScheduler.handleResponse()
	}

	return f.taskChannel
}