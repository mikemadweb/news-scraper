package scheduler

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"bitbucket.org/mikemadweb/news-scraper/app/pool/worker"
	"bitbucket.org/mikemadweb/news-scraper/config/custom"
	"bitbucket.org/mikemadweb/news-scraper/config/source"
)

func Test_state_StoreNewArticleLinks(t *testing.T) {
	output := worker.ExtractLinksOutput{
		PageUrl: "https://example.com/bike?angle=breath",
		ArticleLinks: []string{
			"http://example.com/boat/boundary.aspx?back=bedroom",
			"http://example.com/airplane",
			"https://www.example.net/breath?brass=behavior#brother",
		},
	}
	s := newState(&source.Settings{
		Domain:               "example.com",
		RequestRatePerMinute: 12,
		List:                 source.List{
			Pages: []custom.Url{
				"https://example.com/bike?angle=breath",
			},
		},
		Article:              source.Article{},
	})

	assert.Equal(t, s.newArticleQueue.Len(), 0)
	assert.NoError(t, s.StoreNewArticleLinks(output))
	assert.Equal(t, s.newArticleQueue.Len(), 3)
}

func Test_state_StoreVisitedArticle(t *testing.T) {
	output := worker.ScrapeArticleOutput{
		ArticleUrl: "https://example.com/bike?angle=breath",
		Version: "1fce29af4980b1296cb6e8d1cd848b04",
	}
	s := newState(&source.Settings{
		Domain:               "example.com",
		RequestRatePerMinute: 12,
		List:                 source.List{
			Pages: []custom.Url{
				"https://example.com/bike?angle=breath",
			},
		},
		Article:              source.Article{},
	})
	s.foundArticleMap["https://example.com/bike?angle=breath"] = ""

	assert.Equal(t, s.visitedArticleQueue.Len(), 0)
	assert.NoError(t, s.StoreVisitedArticle(output))
	assert.Equal(t, s.visitedArticleQueue.Len(), 1)
}