package scheduler

import (
	"testing"
	"time"

	"bitbucket.org/mikemadweb/news-scraper/app/pool/worker"
	"bitbucket.org/mikemadweb/news-scraper/config/custom"
	"bitbucket.org/mikemadweb/news-scraper/config/source"
)

func Test_scheduler_IsTimeToVisitListPage(t *testing.T) {
	type fields struct {
		sourceSettings         *source.Settings
		state                  *state
		linksResponseChannel   chan worker.ExtractLinksOutput
		articleResponseChannel chan worker.ScrapeArticleOutput
		errorResponseChannel   chan error
	}
	type args struct {
		listPage *listPageState
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   bool
	}{
		{
			name: "unvisited page",
			fields: fields{
				sourceSettings: &source.Settings{
					List: source.List{
						ScrapeInterval: custom.Duration(2 * time.Minute),
					},
				},
			},
			args: args{listPage: &listPageState{
				visitedAt: 0,
			}},
			want: true,
		},
		{
			name: "is time to visit",
			fields: fields{
				sourceSettings: &source.Settings{
					List: source.List{
						ScrapeInterval: custom.Duration(2 * time.Minute),
					},
				},
			},
			args: args{listPage: &listPageState{
				visitedAt: time.Now().Add(-3 * time.Minute).UnixNano(),
			}},
			want: true,
		},
		{
			name: "is not time to visit",
			fields: fields{
				sourceSettings: &source.Settings{
					List: source.List{
						ScrapeInterval: custom.Duration(5 * time.Minute),
					},
				},
			},
			args: args{listPage: &listPageState{
				visitedAt: time.Now().Add(-3 * time.Minute).UnixNano(),
			}},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &scheduler{
				sourceSettings:         tt.fields.sourceSettings,
				state:                  tt.fields.state,
				linksResponseChannel:   tt.fields.linksResponseChannel,
				articleResponseChannel: tt.fields.articleResponseChannel,
				errorResponseChannel:   tt.fields.errorResponseChannel,
			}
			if got := s.IsTimeToVisitListPage(tt.args.listPage); got != tt.want {
				t.Errorf("IsTimeToVisitListPage() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_scheduler_IsTimeToRevisitArticle(t *testing.T) {
	type fields struct {
		sourceSettings         *source.Settings
		state                  *state
		linksResponseChannel   chan worker.ExtractLinksOutput
		articleResponseChannel chan worker.ScrapeArticleOutput
		errorResponseChannel   chan error
	}
	type args struct {
		article *articleState
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   bool
	}{
		{
			name: "is time to visit",
			fields: fields{
				sourceSettings: &source.Settings{
					Article: source.Article{
						ScrapeInterval: custom.Duration(2 * time.Minute),
					},
				},
			},
			args: args{article: &articleState{
				visitedAt: time.Now().Add(-3 * time.Minute).UnixNano(),
			}},
			want: true,
		},
		{
			name: "is not time to visit",
			fields: fields{
				sourceSettings: &source.Settings{
					Article: source.Article{
						ScrapeInterval: custom.Duration(4 * time.Minute),
					},
				},
			},
			args: args{article: &articleState{
				visitedAt: time.Now().Add(-3 * time.Minute).UnixNano(),
			}},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &scheduler{
				sourceSettings:         tt.fields.sourceSettings,
				state:                  tt.fields.state,
				linksResponseChannel:   tt.fields.linksResponseChannel,
				articleResponseChannel: tt.fields.articleResponseChannel,
				errorResponseChannel:   tt.fields.errorResponseChannel,
			}
			if got := s.IsTimeToRevisitArticle(tt.args.article); got != tt.want {
				t.Errorf("IsTimeToRevisitArticle() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_scheduler_IsTimeToVisitArticle(t *testing.T) {
	type fields struct {
		sourceSettings         *source.Settings
		state                  *state
		linksResponseChannel   chan worker.ExtractLinksOutput
		articleResponseChannel chan worker.ScrapeArticleOutput
		errorResponseChannel   chan error
	}
	type args struct {
		article *articleState
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   bool
	}{
		{
			name: "is time to visit",
			args: args{article: &articleState{}},
			want: true,
		},
		{
			name: "is not time to visit",
			args: args{article: nil},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &scheduler{
				sourceSettings:         tt.fields.sourceSettings,
				state:                  tt.fields.state,
				linksResponseChannel:   tt.fields.linksResponseChannel,
				articleResponseChannel: tt.fields.articleResponseChannel,
				errorResponseChannel:   tt.fields.errorResponseChannel,
			}
			if got := s.IsTimeToVisitArticle(tt.args.article); got != tt.want {
				t.Errorf("IsTimeToVisitArticle() = %v, want %v", got, tt.want)
			}
		})
	}
}