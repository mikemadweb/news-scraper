package scheduler

import (
	"fmt"
	"log"
	"time"

	"bitbucket.org/mikemadweb/news-scraper/app/pool/worker"
	"bitbucket.org/mikemadweb/news-scraper/config"
	"bitbucket.org/mikemadweb/news-scraper/config/source"
)

type scheduler struct {
	sourceSettings         *source.Settings
	state                  *state
	linksResponseChannel   chan worker.ExtractLinksOutput
	articleResponseChannel chan worker.ScrapeArticleOutput
	errorResponseChannel   chan error
}

func NewSourceScheduler(settings *source.Settings) *scheduler {
	return &scheduler{
		sourceSettings:         settings,
		state:                  newState(settings),
		linksResponseChannel:   make(chan worker.ExtractLinksOutput),
		articleResponseChannel: make(chan worker.ScrapeArticleOutput),
		errorResponseChannel:   make(chan error),
	}
}

func (s *scheduler) scheduleSend(taskChannel chan<- interface{}) {
	if config.IsDebugMode() {
		log.Printf("scheduler: scheduling start for %s", s.sourceSettings.Domain)
	}

	interval := s.calculateInterval(s.sourceSettings.RequestRatePerMinute)
	for range time.Tick(interval) {
		newTask := s.pickNextTask()
		if newTask != nil {
			taskChannel <- newTask
		}
	}
}

func (s *scheduler) calculateInterval(ratePerMinute uint32) time.Duration {
	return time.Minute / time.Duration(ratePerMinute)
}

func (s *scheduler) pickNextTask() interface{} {
	if s.IsTimeToVisitListPage(s.state.GetCurrentListPage()) {
		if config.IsDebugMode() {
			log.Printf("%s: schedule an extract links task", s.getLoggerPrefix())
		}

		return s.newExtractLinksTask()
	}

	if s.IsTimeToRevisitArticle(s.state.PeekCurrentVisitedArticle()) {
		if config.IsDebugMode() {
			log.Printf("%s: schedule an scrape task for visited article", s.getLoggerPrefix())
		}

		return s.newScrapeArticleTask(s.state.GetCurrentVisitedArticle())
	}

	if s.IsTimeToVisitArticle(s.state.PeekCurrentNewArticle()) {
		if config.IsDebugMode() {
			log.Printf("%s: schedule an scrape task for new article", s.getLoggerPrefix())
		}

		return s.newScrapeArticleTask(s.state.GetCurrentNewArticle())
	}

	if config.IsDebugMode() {
		log.Printf("%s: no task, pending", s.getLoggerPrefix())
	}

	return nil
}

func (s *scheduler) newExtractLinksTask() worker.ExtractLinksTask {
	return worker.ExtractLinksTask{
		Input: worker.ExtractLinksInput{
			PageUrl:           s.state.GetCurrentListPage().url,
			ArticleUrlPattern: s.sourceSettings.List.ArticleUrlPattern,
		},
		Output: s.linksResponseChannel,
		Error:  s.errorResponseChannel,
	}
}

func (s *scheduler) newScrapeArticleTask(article *articleState) worker.ScrapeArticleTask {
	return worker.ScrapeArticleTask{
		Input: worker.ScrapeArticleInput{
			PageUrl:          article.url,
			BodyWordLimit:    s.sourceSettings.Article.BodyWordLimit,
			HeadlineSelector: s.sourceSettings.Article.HtmlSelectors.Headline,
			BodySelector:     s.sourceSettings.Article.HtmlSelectors.Body,
		},
		Output: s.articleResponseChannel,
		Error:  s.errorResponseChannel,
	}
}

func (s *scheduler) handleResponse() {
	go s.handleErrorResponse()

	for {
		select {
		case response := <-s.articleResponseChannel:
			err := s.state.StoreVisitedArticle(response)
			if err != nil {
				s.errorResponseChannel <- err
			}

		case response := <-s.linksResponseChannel:
			err := s.state.StoreNewArticleLinks(response)
			if err != nil {
				s.errorResponseChannel <- err
			}
		}
	}
}

func (s *scheduler) IsTimeToVisitListPage(listPage *listPageState) bool {
	nextPageVisitedAt := listPage.visitedAt
	listPageScrapeInterval := s.sourceSettings.List.ScrapeInterval.Unwrap().Nanoseconds()

	return nextPageVisitedAt == 0 ||
		nextPageVisitedAt+listPageScrapeInterval < time.Now().UnixNano()
}

func (s *scheduler) IsTimeToRevisitArticle(article *articleState) bool {
	if article == nil {
		return false
	}

	nextArticleVisitedAt := article.visitedAt
	articleScrapeInterval := s.sourceSettings.Article.ScrapeInterval.Unwrap().Nanoseconds()

	return nextArticleVisitedAt+articleScrapeInterval < time.Now().UnixNano()
}

func (s *scheduler) IsTimeToVisitArticle(article *articleState) bool {
	return article != nil
}

func (s *scheduler) getLoggerPrefix() string {
	return fmt.Sprintf("scheduler [%s]", s.sourceSettings.Domain)
}

func (s *scheduler) handleErrorResponse() {
	for err := range s.errorResponseChannel {
		if e, ok := err.(worker.Error); ok {
			s.state.ResetArticle(e.Url)
		}

		log.Printf("ERROR [%s]: %s", s.sourceSettings.Domain, err.Error())
	}
}
